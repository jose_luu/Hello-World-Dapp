pragma solidity ^0.7.1;

contract HelloWorld {
    string public greeting = 'Hi';

    function setGreeting(string memory _greeting) public {
        greeting = _greeting;
    }
}
