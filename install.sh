# install node-v17 
# because ubuntu 18 does not have glibc 28 required by node-v18
npm install --save-dev @babel/preset-react
npm install --save react-bootstrap bootstrap
npm install webpack webpack-cli --save-dev
npm install -D babel-loader @babel/core @babel/preset-env
npm install less-loader@6
npm install --save-dev html-webpack-plugin@4
npm install dotenv
npm install truffle@5.3.0
npm install truffle@v5.3.0
