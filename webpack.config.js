const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    entry: ['./src/index.js'],
    mode: 'development',
    optimization: {
        minimize: false
    },
    output: {
        path: __dirname + '/build',
        filename: 'bundle.js'
    },
    
    plugins:[
      new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify('development')
        }
      })
    ],

    module: {
      rules: [
        {
          test: /\.m?js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
		['@babel/preset-env', { 
                  useBuiltIns: 'usage', 
                  corejs: "3",  
                  targets: {
                    browsers: ['last 2 versions'],
              	  },
                 }
	       ],
		 '@babel/preset-react'],
	       plugins: [
		 ['@babel/plugin-transform-runtime']
	       ]
            }
          }
        },
        {
          test: /\.(sass|less|css)$/,
          loaders: ['style-loader', 'css-loader', 'less-loader']
        }
      ]
    }
}
